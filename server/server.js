/**
 * Module dependencies
 */

var express = require('express'),
    bodyParser = require('body-parser'),
    http = require('http'),
    path = require('path'),
    fs = require("fs");
var app = module.exports = express();
app.use( bodyParser.json() );

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.use(express.static(path.join(__dirname, '../app')));

var env = process.env.NODE_ENV || 'development';

/**
 * Routes
 */

// serve index and view partials

/**
 * @method submitContactForm
 * @description api end point for submit contact form
 */
app.post('/submitContactForm', function(req,res){
    try{
        if(req.body.name && req.body.name !== ""){
            var name = req.body.name;
            if(name.indexOf("ei") !== -1 && name.indexOf("cei") === -1){
                res.status(400);
                res.send({error:"names contain a ‘i’ after ‘e’ that is not after a ‘c’ is forbidden"})
            }else{
                fs.appendFile("database/contactFormData.txt",JSON.stringify(req.body) + "\n",function(err){
                    if(err){
                        res.status(400);
                        res.send(err);
                    }else{
                        res.sendStatus(200)
                    }
                });
            }
        }
    }catch (err){
        res.send(err);
        var errorMessage = (new Date()) + err.toString() + "\n";
        fs.appendFile("logFiles/contactForm.log",errorMessage,function(err){
            console.error(err);
        })
    }
});

/**
 * @method submitApplyForm
 * @description api end point for submit apply form
 */
app.post('/submitApplyForm', function(req,res){
    try{
        if(req.body.name && req.body.name !== ""){
            var name = req.body.name;
            if(name.indexOf("ei") !== -1 && name.indexOf("cei") === -1){
                res.status(400);
                res.send({error:"names contain a ‘i’ after ‘e’ that is not after a ‘c’ is forbidden"})
            }else{
                fs.appendFile("database/applyFormData.txt",JSON.stringify(req.body) + "\n",function(err){
                    if(err){
                        res.status(400);
                        res.send(err);
                    }else{
                        res.sendStatus(200)
                    }
                });

            }
        }
    }catch (err){
        res.status(400);
        res.send(err.toString());
        var errorMessage = (new Date()) + err.toString() + "\n";
        fs.appendFile("logFiles/applyForm.log",errorMessage,function(err){
            console.error(err);
        })
    }

});

/**
 * Start Server
 */

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});