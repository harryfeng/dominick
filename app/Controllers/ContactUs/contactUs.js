'use strict';

angular.module('myApp.contactUs', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/contactUs', {
    templateUrl: './Controllers/ContactUs/contactUs.html',
    controller: 'ContactUsCtrl'
  });
}])

.controller('ContactUsCtrl', ["$scope","$http","$window",function($scope,$http,$window) {

        $scope.contactData = {}; //data of the contact form

        $scope.isModalShowing = false; // use this to show/hide modal window

        $scope.isFormShowing = true; // when this is true, form will show

        /**
         * @method submitContactForm
         * @description method call when click submit form button
         */
        $scope.submitContactForm = function(){
            if($scope.contactForm.$invalid){
                return;
            };
            if($scope.contactData.name.match(/\d+/g) !== null){
                console.log("Name should now contain number");
                alert("Name should not contain number");
                return;
            };
            $http({
                method: 'POST',
                url: '/submitContactForm',
                data:$scope.contactData
            }).then(function successCallback(response) {
                $scope.isModalShowing = true;
            }, function errorCallback(response) {
                $scope.isFormShowing = false;
            });

            $window.location = "mailto:jobs@knowroaming.com?from=harry@harry.com";

        };

        $scope.closeModalWindow = function(){
            $scope.isModalShowing = false;
        }


}]);