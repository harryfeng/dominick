'use strict';

angular.module('myApp.applyNow', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/applyNow', {
    templateUrl: './Controllers/ApplyNow/applyNow.html',
    controller: 'ApplyNowCtrl'
  });
}])

.controller('ApplyNowCtrl', ['$scope', "$http", function($scope,$http) {

        $scope.applyFormData = {}; // data of the apply form

        $scope.isFormShowing = true; // when this is true, form will show

        $scope.isSuccessfulSubmit = false; // after submit successfully, this will become true

        $scope.isErrorShowing = false; // when there is an error, this will become true;

        /**
         * @method submitApplyForm
         * @description method call when click submit form button
         */
        $scope.submitApplyForm = function(){
            console.log($scope.applyFormData);
            if($scope.applyForm.$invalid){
                return;
            };

            $http({
                method: 'POST',
                url: '/submitApplyForm',
                data:$scope.applyFormData
            }).then(function successCallback(response) {
                $scope.isFormShowing = false;
                $scope.isSuccessfulSubmit = true;
            }, function errorCallback(response) {
                $scope.isFormShowing = false;
                $scope.isErrorShowing = true;
            });
        }

}]);