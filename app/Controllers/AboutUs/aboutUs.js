'use strict';

angular.module('myApp.aboutUs', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/aboutUs', {
    templateUrl: './Controllers/AboutUs/aboutUs.html',
    controller: 'AboutUsCtrl'
  });
}])

/**
 * @description About us page controller
 */
.controller('AboutUsCtrl', ['$scope',function($scope) {
        /**
         * @method hoverHiddenDiv
         * @description when you click a hidden field, show an alert
         */
        $scope.hoverHiddenDiv = function(){
            console.log("Trigger hover hidden div");
            alert("Hover a hidden div");
        }
}]);