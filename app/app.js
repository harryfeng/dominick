'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.applyNow',
  'myApp.aboutUs',
  'myApp.contactUs',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/aboutUs'});
}])
    .controller('AppCtrl', ['$scope', function($scope) {
        $scope.showMenu = function(){
            console.log("show menu");
            $scope.mobileMenu = "aa"
        }
    }]);
